# workflow

component for workflow

## Installation

```yml
# mlplatform-domain.yml

name: domain
comps:
 - https://gitlab.chriamue.de/mlplatform/comp/workflow.git
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.workflow',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.workflow.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Workflow
} from 'comps/workflow/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Workflow: 'Workflow',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Workflow,
    path: '/workflow',
    component: Workflow,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.Workflow} />
    ...
</div>
```

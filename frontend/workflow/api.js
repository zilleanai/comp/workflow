import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function workflowUri(uri) {
  return v1(`/workflow${uri}`)
}

export default class Workflow {

  static save({ config }) {
    return post(workflowUri(`/save/${storage.getProject()}`), {
      config
    })
  }

  static listConfigs() {
    return get(workflowUri(`/configs`))
  }

  static listConfig({project, domain}) {
    return get(workflowUri(`/config/${project}/${domain}`))
  }

  static listOrigin({ project }) {
    return get(workflowUri(`/origin/${project}`))
  }

}

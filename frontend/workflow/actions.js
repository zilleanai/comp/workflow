import { createRoutine } from 'actions'

export const save = createRoutine('workflow/SAVE')
export const listConfigs = createRoutine('workflow/LIST_CONFIGS')
export const listConfig = createRoutine('workflow/LIST_CONFIG')
export const listOrigin = createRoutine('workflow/LIST_ORIGIN')

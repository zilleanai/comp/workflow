import { listConfig } from 'comps/workflow/actions'


export const KEY = 'config'

const initialState = {
  isLoading: false,
  isLoaded: false,
  workflowConfigProjects: [],
  byWorkflowConfigProject: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { workflowConfigProjects, byWorkflowConfigProject } = state
  switch (type) {
    case listConfig.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listConfig.SUCCESS:
      const id = payload.config.project + payload.config.domain
      if (!workflowConfigProjects.includes(id)) {
        workflowConfigProjects.push(id)
      }
      byWorkflowConfigProject[id] = payload.config.config
      return {
        ...state,
        config: payload.config.config,
        isLoaded: true,
      }

    case listConfig.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listConfig.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectConfig = (state) => state[KEY]
export const selectConfigList = (state, project, domain) => selectConfig(state).byWorkflowConfigProject[project+domain]

import { listConfigs } from 'comps/workflow/actions'


export const KEY = 'configs'

const initialState = {
  isLoading: false,
  isLoaded: false,
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { configs } = payload || {}
  switch (type) {
    case listConfigs.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listConfigs.SUCCESS:
      return {
        ...state,
        configs,
        isLoaded: true,
      }

    case listConfigs.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listConfigs.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectConfigs = (state) => state[KEY]
export const selectConfigsList = (state) => {
  const configs = selectConfigs(state)
  return configs.configs
}
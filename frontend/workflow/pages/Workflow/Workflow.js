import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { v1 } from 'api'
import Helmet from 'react-helmet'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { InfoBox, PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { ConfigList, FeatureSelect } from 'comps/workflow/components'
import { storage } from 'comps/project'
import { save } from 'comps/workflow/actions'
import { listConfig } from 'comps/workflow/actions'
import { selectConfigList } from 'comps/workflow/reducers/config'
import { TagSelect } from 'comps/tag/components'
import ReactJson from 'react-json-view'
import hash from 'object-hash'

class Workflow extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      project: storage.getProject(),
      domain: null,
      config: {},
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.workflow-page',
          intro: 'On this page a config file can be edited.',
        },
        {
          element: '.config-list',
          intro: 'From this list a default config file for a special domain can be selected.',
        },
        {
          element: '.json-editor',
          intro: 'This editor allows to edit configuratin, remove or insert values.',
        },
        {
          element: '.button-primary',
          intro: 'Configuration can be saved pressing this button.',
        },
      ],
    }

    this.inputEl = React.createRef();
  }

  renderConfigs = ({ input: { onChange }, project }) => {
    return (<ConfigList project={project} onChange={onChange} />)
  }

  onConfigsChanged = ({ value: domain }) => {
    this.setState({
      domain
    })
    this.props.listConfig.trigger({ payload: { project: this.state.project, domain } })
  }

  onConfigChanged = (config) => {
    this.setState({ config: config.updated_src })
  }

  componentWillMount() {
    this.props.listConfig.maybeTrigger({ project: this.state.project, domain: this.state.domain })
  }

  componentWillReceiveProps(nextProps) {
    const { listConfig, project, config, domain } = nextProps
    if (project != this.props.project) {
      listConfig.maybeTrigger({ project: this.state.project, domain: this.state.domain })
    }
    this.setState({
      project,
      config,
      domain,
      initialValues: config,
    });
  }

  handleFeatures = ({ value }) => {
    let config = this.state.config
    config.dataset.features = value
    this.setState({ config: config })
  }



  render() {
    const { isLoaded, error, pristine, submitting } = this.props
    const { domain, config } = this.state
    if (!isLoaded || !domain) {
      return null
    }

    console.log(config.dataset.features, hash(config))
    const { stepsEnabled, steps, initialStep } = this.state
    return (
      <PageContent className='workflow-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Workflow</title>
        </Helmet>
        <h1>Workflow!</h1>

        <h5>Configs</h5>
        <div className='config-list'>
          <ConfigList onChange={this.onConfigsChanged}
            defaultValue={domain}
            project={storage.getProject()} />
        </div>
        <div className='json-editor'>
          <ReactJson id={'json-view-'+hash(config)} onEdit={this.onConfigChanged} onAdd={this.onConfigChanged} onDelete={this.onConfigChanged} src={config} />
        </div>

        {config.dataset && config.dataset.features ? <div className='json-editor'>
          <label>Dataset Features</label>
          <TagSelect id={hash(config.dataset.features)} onChange={this.handleFeatures} defaultValue={config.dataset ? config.dataset.features : null} />
        </div> : null}
        <div className="row">
          <button type="submit"
            className="button-primary"
            onClick={(e) => {
              this.props.save.trigger({
                config
              })
            }}
            disabled={pristine || submitting}
          >
            {submitting ? 'Saving...' : 'Save'}
          </button>
        </div>
      </PageContent>)
  }
}

const withConnect = connect(
  (state) => {
    const project = storage.getProject()
    const config = selectConfigList(state, project, state.domain)
    const isLoaded = !!config
    if (isLoaded) {
      const domain = config.domain
      return {
        domain,
        isLoaded,
        project,
        config,
      }
    }
    return {
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ save, listConfig }, dispatch),
)


const withReducer2 = injectReducer(require('comps/workflow/reducers/config'))

const withSaga = injectSagas(require('comps/workflow/sagas/save'))

const withSaga2 = injectSagas(require('comps/workflow/sagas/config'))

export default compose(
  withReducer2,
  withSaga,
  withSaga2,
  withConnect,
)(Workflow)


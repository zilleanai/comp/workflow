import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listConfigs } from 'comps/workflow/actions'
import WorkflowApi from 'comps/workflow/api'
import { selectConfigs } from 'comps/workflow/reducers/configs'

export const KEY = 'configs'


export const maybeListConfigsSaga = function* () {
  const { isLoading, isLoaded } = yield select(selectConfigs)
  if (!(isLoaded || isLoading)) {
    yield put(listConfigs.trigger())
  }
}

export const listConfigsSaga = createRoutineSaga(
  listConfigs,
  function* successGenerator() {
    const configs = yield call(WorkflowApi.listConfigs)

    yield put(listConfigs.success({
      configs,
    }))
  },
)

export default () => [
  takeEvery(listConfigs.MAYBE_TRIGGER, maybeListConfigsSaga),
  takeLatest(listConfigs.TRIGGER, listConfigsSaga),
]

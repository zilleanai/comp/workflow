import { call, put, takeLatest } from 'redux-saga/effects'
import { createRoutineFormSaga } from 'sagas'
import { save } from 'comps/workflow/actions'
import WorkflowApi from 'comps/workflow/api'


export const KEY = 'save'

export const saveSaga = createRoutineFormSaga(
  save,
  function* successGenerator(payload) {
    const response = yield call(WorkflowApi.save, payload)
    yield put(save.success(response))
  }
)

export default () => [
  takeLatest(save.TRIGGER, saveSaga)
]

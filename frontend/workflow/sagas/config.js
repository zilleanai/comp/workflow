import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listConfig } from 'comps/workflow/actions'
import WorkflowApi from 'comps/workflow/api'
import { selectConfig } from 'comps/workflow/reducers/config'

export const KEY = 'config'


export const maybeListConfigSaga = function* (payload) {
  const { byWorkflowConfigProject, isLoading } = yield select(selectConfig)
  const isLoaded = !!byWorkflowConfigProject[payload.project+payload.domain]
  if (!(isLoaded || isLoading)) {
    yield put(listConfig.trigger(payload))
  }
}

export const listConfigSaga = createRoutineSaga(
  listConfig,
  function* successGenerator({ payload: payload }) {
    const config = yield call(WorkflowApi.listConfig, payload)

    yield put(listConfig.success({
      config,
    }))
  },
)

export default () => [
  takeEvery(listConfig.MAYBE_TRIGGER, maybeListConfigSaga),
  takeLatest(listConfig.TRIGGER, listConfigSaga),
]

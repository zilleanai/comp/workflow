import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import Select from 'react-select';
import { listConfigs, listConfig } from 'comps/workflow/actions'
import { selectConfigsList } from 'comps/workflow/reducers/configs'
import { selectConfigList } from 'comps/workflow/reducers/config'
import Checkbox from 'components/Checkbox';
import './config-list.scss'

class ConfigList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checkedItems: new Map(),
      project: props.project,
      selectedOption: null,
      domain: props.defaultValue
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    if (this.props.onChange) {
      this.props.onChange({value:selectedOption.value})
    }
  }

  componentWillMount() {
    this.props.listConfigs.maybeTrigger()
    this.props.listConfig.maybeTrigger({ project: this.props.project, domain: this.state.domain })
  }

  componentWillReceiveProps(nextProps) {
    const { listConfigs, listConfig, project, configs, config } = nextProps
    if (project != this.props.project) {
      listConfigs.maybeTrigger()
      listConfig.maybeTrigger({ project: project, domain: this.state.domain })
    }
    if (nextProps.isLoaded) {
      this.setState({
        project,
        configs,
        config,
      });
    }
  }

  itemsToOptions(items) {
    var options = []
    items.forEach(function (value, key) {
      options.push({ value: value, label: value })
    }, items)
    return options
  }

  selectedOptions(selected, options) {
    var selOptions = []
    if (!!selected && !!options) {
      options.forEach(function (value, key) {
        if (selected.indexOf(value.value) > -1) {
          selOptions.push(value)
        }
      }, options)
    }
    return selOptions
  }

  render() {
    const { isLoaded, error, configs, config, project, selected, defaultValue } = this.props

    if (!isLoaded) {
      return (<div>{project}</div>)
    }
    const options = this.itemsToOptions(configs)
    const dvalue = this.selectedOptions(defaultValue || config.domain, this.itemsToOptions(configs))
    return (
      <Select
        value={this.state.selectedOption || dvalue || null}
        onChange={this.handleChange}
        isSearchable={true}
        isClearable={true}
        name="configs"
        options={options}
        className="basic-single"
        classNamePrefix="select"
      />
    )
  }
}

const withReducer = injectReducer(require('comps/workflow/reducers/configs'))
const withReducer2 = injectReducer(require('comps/workflow/reducers/config'))

const withSaga = injectSagas(require('comps/workflow/sagas/configs'))
const withSaga2 = injectSagas(require('comps/workflow/sagas/config'))

const withConnect = connect(
  (state, props) => {
    const project = props.project
    const selected = props.selected
    const defaultValue = props.defaultValue
    const configs = selectConfigsList(state)
    const config = selectConfigList(state, project, defaultValue)
    const isLoaded = !!configs
    return {
      project,
      configs,
      config,
      selected,
      defaultValue,
      domain: defaultValue,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listConfigs, listConfig }, dispatch),
)

export default compose(
  withReducer,
  withReducer2,
  withSaga,
  withSaga2,
  withConnect,
)(ConfigList)

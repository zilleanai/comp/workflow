import os
import json
import yaml
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
from werkzeug.utils import secure_filename
from zworkflow.config import Config
from zworkflow.configs import configs


class WorkflowController(Controller):

    @route('/configs')
    def configs(self):
        return jsonify(list(configs.keys()))

    @route('/config/<string:project>/<string:domain>')
    def config(self, project, domain):
        if domain == 'null' or domain == 'undefined':
            domain = None
        os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
        configfile = os.path.join('workflow.yml')
        config = None
        if os.path.exists(configfile):
            with open(configfile) as file:
                config = yaml.load(file, Loader=yaml.SafeLoader)
                if domain:
                    config['domain'] = domain
                elif config.get('domain') is None:
                    config['domain'] = 'default'
        else:
            domain = 'default'

        config = Config(config or {'domain': domain})
        return jsonify(project=project, config=config.config)

    @route('/origin/<string:project>')
    def origin(self, project):
        os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
        configfile = os.path.join('workflow.yml') or {}
        with open(configfile, 'r') as stream:
            try:
                workflow = yaml.load(stream, Loader=yaml.SafeLoader)
            except yaml.YAMLError:
                return abort(404)
        return jsonify(project=project, config=workflow)

    @route('/save/<string:project>', methods=['POST'])
    def save(self, project):
        os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
        configfile = os.path.join('workflow.yml')
        config = request.json['config']
        with open(configfile, 'w') as outfile:
            yaml.dump(config,
                      outfile, default_flow_style=False)
        return jsonify(success=True)
